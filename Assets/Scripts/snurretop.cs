using UnityEngine;
using System.Collections;

public class snurretop : MonoBehaviour
{

    public float speed = 1.0f;
    public float speed_up = 1.0f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, speed, 0);
        transform.Translate(0, speed_up * Time.deltaTime, 0);
    }
}
