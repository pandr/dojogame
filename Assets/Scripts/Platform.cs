using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour
{

    public float max_hoejde = 4.0f;

    private Vector3 start_position;
    private float retning_y = 1.0f; // start med at koere op!

    void Start()
    {
        start_position = transform.position;
    }

    void Update()
    {
        float hoejde = transform.position.y - start_position.y;

        if (hoejde > max_hoejde)
        {
            retning_y = -1.0f;
        }
        if (hoejde < 0)
        {
            retning_y = 1.0f;
        }

        transform.Translate(0, retning_y * Time.deltaTime, 0);
    }
}
