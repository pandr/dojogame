using UnityEngine;
using System.Collections;

public class playercontroller : MonoBehaviour
{
    public float speed = 1.0f;
    public GameObject model;

    private Vector3 move;

    void Update()
    {
        var cc = GetComponent<CharacterController>();

        // Bevægelse -- nu v.h.a. 
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        move.x = horizontal * speed;
        move.z = vertical * speed;

        // Staar vi paa noget?
        if(cc.isGrounded)
        {
            // Hop!
            if (Input.GetKeyDown(KeyCode.Space))
            {
                move.y = 5.0f;
            }
        }
        else
        {
            // Fald med tyngdekraften
            move.y -= 10.0f * Time.deltaTime;
        }

        cc.Move(move * Time.deltaTime);

        Vector3 horizontal_move = new Vector3(move.x, 0, move.z);
        if(horizontal_move.magnitude > 0.1f && model != null)
            model.transform.forward = Vector3.RotateTowards(model.transform.forward, horizontal_move.normalized, 6.0f*Time.deltaTime, 1.0f);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.transform.GetComponent<GiftRotor>() != null)
            Destroy(other.gameObject);
    }

}
