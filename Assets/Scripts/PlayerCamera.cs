using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{
    public playercontroller player;

    private Vector3 afstand_fra_spiller;

    void Start()
    {
        afstand_fra_spiller = transform.position - player.transform.position;
    }

    void Update()
    {
        Vector3 ny_pos = player.transform.position + afstand_fra_spiller;
        transform.position = ny_pos;
    }
}
