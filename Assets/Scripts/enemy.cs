using UnityEngine;
using System.Collections;

public class enemy : MonoBehaviour
{
    private playercontroller target;
    public Transform spinner;

    void Start()
    {
        target = GameObject.FindObjectOfType<playercontroller>();
    }

    void Update()
    {

        Vector3 vector_to_target = target.transform.position - transform.position;

        float distance_to_target = vector_to_target.magnitude;

        if (distance_to_target < 10.0f)
        {
            spinner.Rotate(0, 500.0f*Time.deltaTime, 0);
            Vector3 direction_to_target = vector_to_target.normalized;
            transform.Translate(2.0f * direction_to_target * Time.deltaTime);
        }
        if(distance_to_target < 1.1f)
        {
            var s = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
            UnityEngine.SceneManagement.SceneManager.LoadScene(s.name);
        }

    }
}
