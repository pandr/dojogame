using UnityEngine;
using System.Collections;

public class spawner : MonoBehaviour
{
    public GameObject prefab;

    void Start()
    {
        for (var x = 0; x < 5; x++)
        {
            var p = new Vector3(x, 0, 0);
            Instantiate(prefab, transform.position + p, Quaternion.identity); // Lav en klon af Prefab
            p = new Vector3(x, 4, 0);
            Instantiate(prefab, transform.position + p, Quaternion.identity); // Lav en klon af Prefab
        }
        for (var y = 0; y < 5; y++)
        {
            var p = new Vector3(0, y, 0);
            Instantiate(prefab, transform.position + p, Quaternion.identity); // Lav en klon af Prefab
            p = new Vector3(4, y, 0);
            Instantiate(prefab, transform.position + p, Quaternion.identity); // Lav en klon af Prefab
        }
        Destroy(gameObject); // Slet dette objekt/script
    }
}
