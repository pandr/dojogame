﻿using UnityEngine;
using System.Collections;

public class TreeCollider : MonoBehaviour
{

    public ParticleSystem effect;
    public GameObject model;

    private float bounce_effect = 0.0f;

    void Update()
    {

        if(bounce_effect > 0.0f)
        {
            float bounce = Mathf.Sin(Time.realtimeSinceStartup * 20.0f) * 0.25f;
            model.transform.localScale = new Vector3(1.0f, bounce_effect * bounce + 1.0f, 1.0f);
            bounce_effect -= Time.deltaTime;
        }

    }

    void OnTriggerEnter(Collider c)
    {
        bounce_effect = 1.0f;
        effect.Play();
    }
}
