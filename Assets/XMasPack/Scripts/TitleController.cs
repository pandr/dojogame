﻿using UnityEngine;
using System.Collections;

public class TitleController : MonoBehaviour
{
    void Update()
    {
        if (Time.timeSinceLevelLoad > 1.0f)
        {
            var cg = GetComponent<CanvasGroup>();
            cg.alpha -= Time.deltaTime;

            // For en ordens skyld: når vi når 0 stopper vi os selv
            if(cg.alpha <= 0.0f)
            {
                enabled = false;
            }
        }
    }
}
