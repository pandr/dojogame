﻿using UnityEngine;
using System.Collections;

public class GiftRotor : MonoBehaviour
{

    public GameObject giftObject;

    private Transform player;

    void Start()
    {
        // Find spilleren
        var pl = GameObject.FindGameObjectWithTag("Player");
        if(pl != null)
        {
            player = pl.transform;
        }

        // Start med tilfældig rotation
        gameObject.transform.Rotate(0, Random.Range(0, 180), 0);
    }

    void Update()
    {
        // Pakken roterer og hopper op og ned hele tiden
        giftObject.transform.Rotate(0, Time.deltaTime * 100.0f, 0);
        giftObject.transform.localPosition = new Vector3(0, Mathf.Sin(Time.realtimeSinceStartup * 2.0f) * 0.15f, 0);

        // Hvis der er en spiller
        if(player != null)
        {
            // Og hvis spilleren er i nærheden
            var to_player = player.transform.position - transform.position;
            if (to_player.magnitude < 5.0f)
            {
                FlyTowardsPlayer();
            }
        }
    }
    
    void FlyTowardsPlayer()
    {
        // Sug os hen til spilleren...
        var to_player = player.transform.position - transform.position;
        float speed = 1.0f - to_player.magnitude / 5.0f;
        Vector3 to_player_norm = to_player.normalized;
        Vector3 movement = 2.0f * to_player_norm;
        movement += new Vector3(-to_player_norm.z, to_player_norm.y, to_player_norm.x);
        movement += new Vector3(
            0,
            1.0f + Mathf.Sin(Time.realtimeSinceStartup * 6.0f) * 2.0f,
            0);
        transform.Translate(4.0f*speed * movement * Time.deltaTime, Space.World);
    }
}
